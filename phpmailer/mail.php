<?php 
// Can be transform using Class instead of function.
// Later can be use soemthing like 
// Mail::send($to, $subject, $message);

require("phpmailer/class.phpmailer.php");
 
function _phpmailer($to, $subject, $message) {
	$sender = "postmaster@mail.my";

	$header = "X-Mailer: PHP/".phpversion() . "Return-Path: $sender";

	$mail = new PHPMailer();

	$mail->IsSMTP();
	$mail->Host = "postmaster.mail.my"; 
	$mail->SMTPAuth = true;
	$mail->Username = 'xxxxx@mail.com';
	$mail->Password = 'xxxx';

	$mail->From = $sender;
	$mail->FromName = "Me";

	$mail->AddAddress($to);                  // name is optional

	$mail->IsHTML(true);                                  // set email format to HTML
	$mail->CreateHeader($header);

	$mail->Subject = $subject;
	$mail->Body    = nl2br($message);
	$mail->AltBody = nl2br($message);

	$mail->Send();	
}

?>