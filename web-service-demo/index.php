<?php 

require_once 'vendor/autoload.php'; 
require_once 'config/init.php';

echo $twig->render('index.html', array(
	'title' => 'Web Service Demo'
));