<?php

$daylen = 60*60*24;

$date1 = '2015-05-04 11:43:52';
$date2 = date("Y-m-d H:i:s");

$days = floor((strtotime($date2) - strtotime($date1)) / $daylen);

echo $days;