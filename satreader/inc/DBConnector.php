<?php

/**
 * A class to connect to MySQL - using adodb5
 *
 * @author Nasrul H. Mohamad <nasrul_hazim@live.com>
 * @version 1.0
 * @package inc
 */
 
class DBConnector
{
	/** 
	 * Static public method to connect to database using adodb5
	 * 
	 * @param $username Username of MySQL user
	 * @param $password Password of MySQL user based on $username
	 * @param $host Location of the database, may required to connect to different server, default connect to localhost / 127.0.0.1
	 * @param $db Specifiy which database to use, default is empty, not selecting any database
	 * @param $driver Type of database to use, default is `mysqlt`
	 *
	 * @return $database Connection of the database
	 */
	static public function getDatabase($username = 'root', $password = '', $host = '127.0.0.1', $db = NULL, $driver = NULL, $port = 5432)
	{
		include_once str_replace('\\','/', dirname(__FILE__) . '/adodb5/adodb.inc.php');
		
		$driver = empty($driver) ? 'mysqlt':$driver; 
			
		$database = ADONewConnection($driver); # create a connection
		$database->NConnect($host, $username, $password, $db);# connect to MySQL
		$database->Execute("SET NAMES 'utf8'");
		
		return $database;
	}
}

?>