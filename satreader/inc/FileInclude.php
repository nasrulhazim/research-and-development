<?php

class FileInclude
{
	static function includeFiles($list)
	{
		if(is_array($list) && count($list) > 0)
		{
			foreach($list as $key => $value)
			{	
				try
				{
					include_once $value . '.php';
				}
				catch(Exception $exception)
				{
					
				}
			}
		}
	}
}

?>