<?php

class StatConverter {
	public $directory;
	public $conn;
	public $log;

	public function init() {

		if(!$this->conn) { exit('No database connection available'); }

		if(empty($this->directory)) { $this->directory = '.'; }

		$files = array();
		if ($handle = opendir($this->directory)) {
		    while (false !== ($entry = readdir($handle))) {
		        if ($entry != "." && $entry != "..") {
		            $files[] = $this->directory . '/' . $entry;
		        }
		    }
		    closedir($handle);
		}

		foreach ($files as $key => $value) {
			$this->readStat($value);
		}

		$this->displayLog();
	}

	private function writeLog($value) {
		if(!$this->log) { $this->log = array(); }
		$this->log[] = $value;
	}

	private function displayLog() {
		if(is_array($this->log)) {
			foreach ($this->log as $key => $value) {
				echo $value . '<hr>';
			}	
		}
	}

	private function readStat($file) {
		$handle = fopen($file, "r");
		if ($handle) {
		    while (($line = fgets($handle)) !== false) {
		        $values = explode(',', $line);
		        $this->addData($values);
		    }
		} else {
		    echo '<b>error opening the file</b>';
		}
	}
	
	private function addData($value) {

		if(is_array($value) && count($value) > 0)
		{
			$type = trim($value[0]);
			switch ($type) {
				case '$POS':
					$this->addPos($value);
					break;
				case '$VELACC':
					$this->addVelacc($value);
					break;
				case '$CLK':
					$this->addClk($value);
					break;
				case '$ION':
					$this->addIon($value);
					break;
				case '$TROP':
					$this->addTrop($value);
					break;
				case '$HWBIAS':
					$this->addHwbias($value);
					break;
				case '$SAT':
					$this->addSat($value);
					break;
				default:
					// do nothing
					break;
			}	
		}
		
	}

	private function addPos($value) {
		unset($value[0]); // remove value '$POS'

		$sql = 'INSERT INTO `utm_gis`.`utm_pos` (`week`, `tow`, `stat`, `posx`, `posy`, `posz`, `posxf`, `posyf`, `poszf`) VALUES (?,?,?,?,?,?,?,?,?)';
		
		$this->_doInsertion($sql,$value);
	}

	private function addVelacc($value) {
		unset($value[0]); // remove value '$POS'

		$sql = 'INSERT INTO `utm_gis`.`utm_velacc` ( `week`, `tow`, `stat`, `vele`, `veln`, `velu`, `acce`, `accn`, `accu`, `velef`, `velnf`, `veluf`, `accef`, `accnf`, `accuf`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)';
		
		$this->_doInsertion($sql,$value);
	}

	private function addClk($value) {
		unset($value[0]); // remove value '$POS'

		$sql = 'INSERT INTO `utm_gis`.`utm_clk` ( `week`, `tow`, `stat`, `rcv`, `clk1`, `clk2`, `clk3`, `clk4` ) VALUES (?,?,?,?,?,?,?,?)';
		
		$this->_doInsertion($sql,$value);
	}

	private function addIon($value) {
		unset($value[0]); // remove value '$POS'

		$sql = 'INSERT INTO `utm_gis`.`utm_ion` ( `week`, `tow`, `stat`, `sat`, `az`, `el`, `ion`, `ion_fixed` ) VALUES (?,?,?,?,?,?,?,?)';
		
		$this->_doInsertion($sql,$value);
	}

	private function addTrop($values) {
		unset($value[0]); // remove value '$POS'

		$sql = 'INSERT INTO `utm_gis`.`utm_trop` ( `week`, `tow`, `stat`, `rcv`, `ztd`, `ztdf` ) VALUES (?,?,?,?,?,?)';
		
		$this->_doInsertion($sql,$value);
	}

	private function addHwbias($values) {
		unset($value[0]); // remove value '$POS'

		$sql = 'INSERT INTO `utm_gis`.`utm_hwbias` ( `week`, `tow`, `stat`, `frg`, `bias`, `biasf` ) VALUES (?,?,?,?,?,?)';
		
		$this->_doInsertion($sql,$value);
	}

	private function addSat($values) {
		unset($value[0]); // remove value '$POS'

		$sql = 'INSERT INTO `utm_gis`.`utm_sat` (`week`,`tow`,`stat`,`frq`,`az`,`el`,`resp`,`resc`,`vsat`,`snr`,`fix`,`slip`,`lock`,`outc`,`slipc`,`rejc`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
		
		$this->_doInsertion($sql,$value);
	}

	private function _doInsertion($sql,$values) {
		try {
			$this->conn->Execute($sql,$values);
		} catch (Exception $e) {
			$this->writeLog($e->getMessage());
		}
	}
}

?>